use amethyst::{
    assets::{AssetStorage, Loader},
    core::transform::{Parent, Transform},
    ecs::prelude::Entity,
    prelude::*,
    renderer::{Camera, PngFormat, Projection, Texture, TextureHandle, TextureMetadata},
    utils::fps_counter::FPSCounter,
};

use crate::orbital::{Mass, Orbit};

pub const SYSTEM_HEIGHT: f32 = 450_000_000_000.0;
pub const SYSTEM_WIDTH: f32 = 450_000_000_000.0;

#[derive(Default)]
pub struct SolarSystemState {}

impl SimpleState for SolarSystemState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        world.register::<Orbit>();
        initialise_planets(world);
        initialise_camera(world);
    }

    // fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
    //     let world = &data.world;
    //     let fps = world.read_resource::<FPSCounter>().sampled_fps();
    //     println!("FPS: {:.*}", 2, fps);
    //     Trans::None
    // }
}

fn load_texture(world: &mut World, name: &str) -> TextureHandle {
    let loader = world.read_resource::<Loader>();
    let texture_storage = world.read_resource::<AssetStorage<Texture>>();
    loader.load(
        format!("texture/{}.png", name),
        PngFormat,
        TextureMetadata::srgb_scale(),
        (),
        &texture_storage,
    )
}

fn initialise_camera(world: &mut World) -> Entity {
    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, 0.0, 1.0);

    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            -SYSTEM_WIDTH / 2.0,
            SYSTEM_WIDTH / 2.0,
            -SYSTEM_HEIGHT / 2.0,
            SYSTEM_HEIGHT / 2.0,
        )))
        .with(transform)
        .build()
}

fn initialise_planets(world: &mut World) {
    let sun = initialise_planet(
        world,
        "sun",
        2_300_000_000.0,
        Mass::new(1.988e30),
        None,
        None,
    );
    let earth = initialise_planet(
        world,
        "earth",
        700_000_000.0,
        Mass::new(5.972e29),
        Some(Orbit::new(0.167, 149_598_261_000.0)),
        Some(Parent::new(sun)),
    );
    initialise_planet(
        world,
        "moon",
        300_000_000.0,
        Mass::new(7.348e22),
        Some(Orbit::new(0.055, 30_847_480_000.0)),
        Some(Parent::new(earth)),
    );
}

fn initialise_planet(
    world: &mut World,
    texture: &str,
    radius: f64,
    mass: Mass,
    orbit: Option<Orbit>,
    parent: Option<Parent>,
) -> Entity {
    let mut builder = world.create_entity().with(Transform::default()).with(mass);
    if let Some(o) = orbit {
        builder = builder.with(o);
    }
    if let Some(p) = parent {
        builder = builder.with(p);
    }
    let planet_container = builder.build();

    let texture = load_texture(world, texture);
    world
        .create_entity()
        .with(texture)
        .with(Parent::new(planet_container))
        .with(scale(radius as f32))
        .build();
    planet_container
}

fn scale(scale: f32) -> Transform {
    let mut transform = Transform::default();
    transform.set_scale(scale, scale, 1.0);
    transform
}