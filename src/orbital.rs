use amethyst::{
    core::timing::Time,
    core::transform::{Parent, Transform},
    ecs::prelude::{Component, DenseVecStorage, Join, Read, ReadStorage, System, WriteStorage},

};

use std::f64::consts::PI;

const TAU: f64 = 2.0 * PI as f64;
const SECONDS_IN_A_DAY: f64 = 60.0 * 60.0 * 24.0;
const G: f64 = 6.67384e-11;
const TIME_SCALE: f64 = 20.0;

pub struct Orbit {
    pub eccentricity: f64,
    pub semimajor_axis: f64,
}

pub struct Mass {
    pub mass: f64,
}

impl Mass {
    pub fn new(mass: f64) -> Mass {
        Mass { mass }
    }
}

impl Component for Mass {
    type Storage = DenseVecStorage<Self>;
}

impl Orbit {
    pub fn new(eccentricity: f64, semimajor_axis: f64) -> Orbit {
        Orbit {
            eccentricity,
            semimajor_axis,
        }
    }
}

impl Component for Orbit {
    type Storage = DenseVecStorage<Self>;
}

pub struct OrbitSystem;

impl<'s> System<'s> for OrbitSystem {
    type SystemData = (
        ReadStorage<'s, Orbit>,
        ReadStorage<'s, Parent>,
        ReadStorage<'s, Mass>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>,
    );

    fn run(&mut self, (orbits, parents, masses, mut locals, time): Self::SystemData) {
        for (orbit, parent, local) in (&orbits, &parents, &mut locals).join() {
            let parent_mass = masses.get(parent.entity).unwrap();
            let t = time.absolute_time().as_millis() as f64 / 1000.0 * TIME_SCALE;
            let position = calculate_position_at_time(orbit, parent_mass.mass, t);
            local.set_translation_xyz(position.0 as f32, position.1 as f32, 0.0);
        }
    }
}


fn calculate_period(semimajor_axis: f64, parent_mass: f64) -> f64 {
    (TAU / SECONDS_IN_A_DAY) * (semimajor_axis.powi(3) / (G * parent_mass)).sqrt()
}

fn calculate_mean_motion(period: f64) -> f64 {
    TAU / period
}

fn calculate_mean_anomaly(mean_motion: f64, time: f64) -> f64 {
    mean_motion * time
}

fn calculate_eccentric_anomoly(eccentricity: f64, mean_anomoly: f64) -> f64 {
    let iterations = 5;
    let e = eccentricity;
    let ma = mean_anomoly;
    let mut ea = ma;
    // using Newton's method here
    for _i in 0..iterations {
        ea = ea - (ea - e * ea.sin() - ma) / (1.0 - e * ea.cos());
    }
    ea
}

fn calculate_true_anomaly(eccentricity: f64, eccentric_anomaly: f64) -> f64 {
    let e = eccentricity;
    let ea = eccentric_anomaly;
    2.0 * (((1.0 + e) / (1.0 - e) * ((ea / 2.0).tan()).powi(2)).sqrt()).atan()
}

fn calculate_heliocentric_distance(
    semimajor_axis: f64,
    eccentricity: f64,
    true_anomaly: f64,
) -> f64 {
    let semilatus_rectum = semimajor_axis * (1.0 - eccentricity.powi(2));
    semilatus_rectum / (1.0 + eccentricity * true_anomaly.cos())
}

fn calculate_position(
    true_anomaly: f64,
    heliocentric_distance: f64,
    period: f64,
    time: f64,
) -> (f64, f64) {
    let ymod = if (time % period) < (period / 2.0) {
        -1.0
    } else {
        1.0
    };

    let x = heliocentric_distance * true_anomaly.cos();
    let y = heliocentric_distance * true_anomaly.sin() * ymod;
    (x, y)
}

fn calculate_position_at_time(orbit: &Orbit, parent_mass: f64, time: f64) -> (f64, f64) {
    let period = calculate_period(orbit.semimajor_axis, parent_mass);
    let mean_motion = calculate_mean_motion(period);
    let mean_anomoly = calculate_mean_anomaly(mean_motion, time);
    let eccentric_anomaly = calculate_eccentric_anomoly(orbit.eccentricity, mean_anomoly);
    let true_anomaly = calculate_true_anomaly(orbit.eccentricity, eccentric_anomaly);
    let heliocentric_distance =
        calculate_heliocentric_distance(orbit.semimajor_axis, orbit.eccentricity, true_anomaly);
    calculate_position(true_anomaly, heliocentric_distance, period, time)
}