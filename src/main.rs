extern crate amethyst;
extern crate rand;

mod orbital;
mod space;

use crate::orbital::OrbitSystem;

use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage},
    utils::{application_root_dir, fps_counter::FPSCounterBundle},
};

use space::SolarSystemState;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let config_path = format!(
        "{}/resources/display_config.ron",
        application_root_dir()?.to_str().unwrap()
    );
    let config = DisplayConfig::load(&config_path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target(
                [
                    0.259f32.powf(2.4),
                    0.329f32.powf(2.4),
                    0.329f32.powf(2.4),
                    1.0,
                ],
                1.0,
            )
            .with_pass(DrawFlat2D::new()),
    );

    let game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(TransformBundle::new())?
        .with_bundle(FPSCounterBundle::default())?
        .with(OrbitSystem, "orbit_system", &[]);
    let mut game = Application::new("./", SolarSystemState::default(), game_data)?;

    game.run();

    Ok(())
}
